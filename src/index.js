import React from 'react';
import ReactDOM from 'react-dom/client';
import Formulario from './components/Form';
import NewEvento from './components/Evento'
import GlobalStyle from './config/globalStyled';
import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <GlobalStyle/>
    <NewEvento /> 
    <Formulario />   
  </React.StrictMode>
);

