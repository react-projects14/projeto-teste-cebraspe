import React, { useState, useEffect } from "react";
import { Container } from "reactstrap";
import { Botao, ButtonIcon } from "./styles";
import FormEvento from "../FormEvento";
import axios from "axios";

const NewEvento = () => {
  const [evento, setEvento] = useState([]);
  const [feedback, setFeedback] = useState(false);

  useEffect(() => {
    axios
      .get("https://extranet.cebraspe.org.br/AvaliacaoCSA/BackEnd/1")
      .then((res) => {
        setEvento(res.data);
        console.log(res.data);
      })
      .catch((error) => console.log(error));
  }, []);

  const handleSubmit = () => {
    setFeedback(true);
  }

  return (
    <>
      <Container>
        <Botao onClick={handleSubmit}>
         Buscar evento <ButtonIcon />
        </Botao>

        <div style={{ color: "#FFF" }}>
        {feedback && <FormEvento data={evento}/>}
      </div>

      </Container>
    </>
  );
};

export default NewEvento;
