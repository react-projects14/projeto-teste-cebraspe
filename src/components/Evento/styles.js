import styled from "styled-components";
import { Button } from "reactstrap";
import { FaSearch } from "react-icons/fa";

export const Botao = styled(Button)`
  background-color: #00dcc9;
  width: 60%;
  height: 40px;
  border-radius: 7px;
  margin-top: 22px;
`;

export const ButtonIcon = styled(FaSearch)`
  font-size: 20px;
  font-weight: 900;
  color: #FFF;
`;
