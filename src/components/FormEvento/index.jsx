import React from "react";
import styled from "styled-components";

const FormEvento = ({data}) => {

  var [dados] = data.map(function(item){return item.message});

  return (
    <>
    <QuotationContainer>
    <Item>Nome: {dados.nomeEvento}</Item>
    <Item>WebSite: {dados.webSite}</Item>
    <Item>Data: {dados.data}</Item>
    <Item>Numero máximo de candidatos: {dados.numeroMaxCandidato}</Item>
    <Item>Bairro: {dados.endereco.bairro}</Item>
    <Item>Logradouro: {dados.endereco.logradouro}</Item>
    <Item>Cidade: {dados.endereco.cidade}</Item>
    <Item>Complemento: {dados.endereco.complemento}</Item>
    <Item>Número: {dados.endereco.numero}</Item>
    <Item>UF: {dados.endereco.uf}</Item>
    <Item>CEP: {dados.endereco.cep}</Item>
    <Item><img src={dados.imageUrl}></img></Item>
    </QuotationContainer>
    <br/>
    </>

  )
}

export default FormEvento;

const QuotationContainer = styled.div`
  padding: 10px;
  border: 1px solid #FFF;
  display: flex;
  flex-direction: column;
  margin-top: 30px;
  background-color: #f9ce89;
  border-radius: 5px;
`;

const Item = styled.div`
  flex: 1;
`;
