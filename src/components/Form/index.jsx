import React from "react";
import {Form, FormGroup, Label, Input, Container} from "reactstrap";
import {Titulo} from "./styles";


function Formulario() {
  return (
    <>
    <Container>
      <Titulo>Cadastrar novo evento</Titulo>
      <Form>
        <FormGroup>
          <Label for="nomeEvento">
           Nome do evento
         </Label>
          <Input
            id="nomeEvento"
            name="nomeEvento"
            placeholder="Nome do Evento"
            type="text"
          />
        </FormGroup>
          <FormGroup>
            <Label for="webSite">
              Website
            </Label>
            <Input
              id="webSite"
              name="webSite"
              placeholder="Digite o nome do webSite"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <Label for="Data das provas">
              Data das provas
            </Label>
            <Input
              id="dataProva"
              name="dataProva"
              placeholder="Escolha a data das provas"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <Label for="Número máximo de candidatos">
              Máximo de candidatos
            </Label>
            <Input
              id="dataProva"
              name="dataProva"
              placeholder="Escolha a data das provas"
              type="text"
            />
          </FormGroup>

          <FormGroup>
            <Label>
              CEP
              <Input
              name="cep"
              maxLength={8}
              //value={this.state.cep}
              //onChange={this.handleChange}
            />
            </Label>
            
          </FormGroup>
          <FormGroup>
            <Label for="logradouro">
            Logradouro
            </Label>
            <Input
              id="logradouro"
              name="logradouro"
              placeholder="logradouro"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <Label for="cidade">
            Cidade
            </Label>
            <Input
              id="cidade"
              name="cidade"
              placeholder="Digite o nome da sua cidade"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <Label for="bairro">
              Bairro
            </Label>
            <Input
              id="bairro"
              name="bairro"
              placeholder="Digite o nome do bairro"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <Label for="uf">
              UF
            </Label>
            <Input
              id="uf"
              name="uf"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <Label for="numero">
              Numero
            </Label>
            <Input
              id="numero"
              name="numero"
              type="text"
            />
          </FormGroup>
          <FormGroup>
            <Label for="complemento">
              Complemento
            </Label>
            <Input
              id="complemento"
              name="complemento"
              type="text"
            />
          </FormGroup>
        </Form>
      </Container>
      <br/>
            </>
   
  )
}

export default Formulario;