import styled from "styled-components";

export const Titulo = styled.div`
  font-size: 20px;
  flex: 1;
  padding-top: 50px;
  padding-bottom: 10px;
`;